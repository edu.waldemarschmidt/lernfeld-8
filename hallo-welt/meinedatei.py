import unittest
from hallo_welt import HalloWelt


class abc(unittest.TestCase):
    def test_passes_hallo_welt(self):
        hw = HalloWelt()
        self.assertEqual(hw.message,'Hallo Welt!')
    def test_fails_hallo_welt(self):
        hw = HalloWelt()
        self.assertEqual(hw.message, 'Hello World!')

if __name__ == '__main__':
    unittest.main(verbosity=2)
