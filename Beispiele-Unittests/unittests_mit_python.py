import unittest

def hello_world():
    return 'hello world'

def erzeuge_list_mit_vorgegener_laenge(laenge):
    return "0123456789"

def entferne_alle_leerzeichen_aus_einem_string(leerzeichen_string):
    wort = leerzeichen_string.replace(" ", "")
    return wort

def erzeuge_eine_sortierte_list_ohne_duplikate(liste):
    wort = set(liste)
    wort = sorted(wort)
    print ('Sortierte Liste ohne Duplikate: ', wort)   
    return wort

def alles_gross(wort):
    return wort.upper()

def ist_true(wert):
    return wert

def ist_gegenwert(wert):
    if wert==True:
        wert = False
    return wert

def ist_primzahl(n):
    print('Aus ist_primzahl: ', n)
    for i in range(3, n):
        if n % i == 0:
            return False
    return True

def um_einen_kleiner(zahl):
    zahl = zahl -300
    return zahl

class TestBeispieleTestCase(unittest.TestCase):
    def test_hello(self):
        self.assertEqual(hello_world(), 'hello world')

    def test_erzeuge_list_mit_vorgegener_laenge(self):
        ergebnis = len(erzeuge_list_mit_vorgegener_laenge(10))
        self.assertEqual(ergebnis, 10)

    def test_entferne_alle_leerzeichen_aus_einem_string(self):
        ergebnis = entferne_alle_leerzeichen_aus_einem_string("hallo  w   e lt")
        self.assertEqual(ergebnis, "hallowelt")

    def test_erzeuge_eine_sortierte_list_ohne_duplikate(self):
        liste = [1,2,2,4,-1,0,0]
        ergebnis = erzeuge_eine_sortierte_list_ohne_duplikate(liste)
        self.assertEqual(ergebnis,[-1,0,1,2,4])
        
    def test_upper(self):
        self.assertEqual(alles_gross('fOo'), 'FOO')
    
    def test_ist_true(self):
        self.assertTrue(ist_true(1==1))
    
    def test_ist_gegenwert(self):
        self.assertFalse(ist_gegenwert(True))
        
    def test_ist_groesser(self):
        z = 6
        self.assertGreater (z,um_einen_kleiner(z))
     
    def setUp(self):
        '''
        Diese Funktion wird vor der Ausführung jedes Tests ausgeführt.
        In diesem Beispiel wird eine Liste aus Wertepaaren erzeugt. Diese ist nur für den Test ist_primzahl relevant. 
        '''
        self.elements=((2,True), (6,False), (10,False))
        #print("test")
        
    def tearDown(self):
        '''
        Diese Funktion wird nach der Ausführung jedes Test ausgeführt. Damit kann nach jedem Test "aufgeräumt" werden.
        Das kann das Schließen einer Datenbankverbindung sein. Oder Zurücksetzen der Änderungen am Dateisystem.
        
        '''
        pass
    
    def test_ist_primzahl(self):
        for (i,erg) in self.elements:
            self.assertEqual(ist_primzahl(i),erg)


# Verbosity=2 sorgt dafür, dass die Ausgaben "wortreicher" sind.
# Wenn Sie den Level auf 1 ändern, erkennen Sie den Unterschied.
if __name__=='__main__':
    unittest.main(argv=[''], verbosity=2, exit=False)


# weitere Testmöglichkeiten: https://moodle.itech-bs14.de/mod/page/view.php?id=33421&amp;forceview=1
